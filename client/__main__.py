import zlib
import json
import socket
import select
from datetime import datetime
from yaml import load, Loader
from argparse import ArgumentParser
import hashlib

from settings import (
    ENCODING_NAME, VARIABLE, HOST,
    PORT, BUFFERSIZE
)


host = HOST
port = PORT
encoding_name = ENCODING_NAME
buffersize = BUFFERSIZE

parser = ArgumentParser()
parser.add_argument(
    '-c', '--config', type=str,
    help='Sets run configuration'
)
parser.add_argument(
    '-m', '--mode', type=str, default='w'
)
args = parser.parse_args()

if args.config:
    with open(args.config) as file:
        config = load(file, Loader=Loader)
        host = config.get('host') or HOST
        port = config.get('port') or PORT
        encoding_name = config.get('encoding_name') or ENCODING_NAME
        buffersize = config.get('buffersize') or BUFFERSIZE

try:
    sock = socket.socket()
    sock.connect((host, port))
    print(f'Client started with { host }:{ port }')

    if args.mode == 'w':
        while True:
            hash_obj = hashlib.sha256()
            hash_obj.update(
                str(datetime.now().timestamp()).encode(ENCODING_NAME)
            )

            action = input('Enter action to send:')
            data = input('Enter data to send:')

            request_string = json.dumps(
                {
                    'action': action,
                    'time': datetime.now().timestamp(),
                    'user': hash_obj.hexdigest(),
                    'data': data
                }
            )

            compressed_request = zlib.compress(
                request_string.encode(ENCODING_NAME)
            )

            sock.send(compressed_request)

    else:
        while True:
            rlist, wlist, xlist = select.select(
                [], [sock], [], 0
            )

            for conn in wlist:
                data = sock.recv(BUFFERSIZE) 
                b_response = zlib.decompress(data)

            print(b_response.decode(ENCODING_NAME))
except KeyboardInterrupt:
    print('Client closed')
