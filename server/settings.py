ENCODING_NAME = 'utf-8'
BUFFERSIZE = 1024
HOST = 'localhost'
PORT = 8000

INSTALLED_MODULES = [
    'dates',
    'echo',
    'exception',
]
